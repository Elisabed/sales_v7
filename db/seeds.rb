# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#Employee table population
Employee.create(employee_first_name: 'Ruben', employee_last_name: 'Smith', employee_title: 'Sales Representative', employee_status: 'Active')
Employee.create(employee_first_name: 'Brenda', employee_last_name: 'Mckenzie', employee_title: 'Sales Manager', employee_status: 'Active')
Employee.create(employee_first_name: 'Benjamin', employee_last_name: 'Payne', employee_title: 'Sales Representative', employee_status: 'Active')
#Employee.create(employee_id: 4, employee_first_name: 'John', employee_last_name: 'Smith', employee_title: 'CEO', employee_status: 'Active')

#Customer table population
Customer.create(customer_name: 'Kyle', customer_middle_name: 'Li', customer_last_name: 'Johnson', customer_address:'123 Sunshine St', customer_city: 'Houston', customer_zip_code: 77506, state: 'Texas', customer_phone: 12378912, customer_email: 'Kyle.J@gmail.com', customer_status: 'Active')


#Inventory table population
Inventory.create(make: 'Nissan', model: 'Altima', year: 2015, vin: 'AB124Df90', color: 'Black', quantity: 1, inventory_status: 'In stock')
Inventory.create(make: 'Dodge', model: 'RAM', year: 2015, vin: 'AB124Df53', color: 'Blue', quantity: 1, inventory_status: 'In stock')
Inventory.create(make: 'Honda', model: 'Accord', year: 2014, vin: 'AB1214Df53', color: 'Silver', quantity: 1, inventory_status: 'In stock')
