class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :employee_first_name
      t.string :employee_last_name
      t.string :employee_title
      t.string :employee_status

      t.timestamps null: false
    end
  end
end
