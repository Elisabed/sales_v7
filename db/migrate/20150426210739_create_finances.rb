class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.decimal :interest
      t.decimal :principal
      t.decimal :payment
      t.decimal :interest_rate
      t.decimal :sales_tax
      t.date :start_date

      t.timestamps null: false
    end
  end
end
