class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :make
      t.string :model
      t.date :year
      t.string :vin
      t.string :color
      t.decimal :wholesale_price
      t.decimal :retail_price
      t.integer :quantity
      t.string :inventory_status

      t.timestamps null: false
    end
  end
end
