class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :customer_name
      t.string :customer_middle_name
      t.string :customer_last_name
      t.string :customer_address
      t.string :customer_city
      t.string :customer_zip_code
      t.string :state
      t.string :customer_phone
      t.string :customer_email
      t.string :customer_status
      t.timestamp :customer_date

      t.timestamps null: false
    end
  end
end
