class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :customer_id
      t.integer :employee_id
      t.integer :inventory_id
      t.timestamp :date
      t.string :quote_status

      t.timestamps null: false
    end
  end
end
