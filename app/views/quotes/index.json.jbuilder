json.array!(@quotes) do |quote|
  json.extract! quote, :id, :customer_id, :employee_id, :inventory_id, :date, :quote_status
  json.url quote_url(quote, format: :json)
end
