json.array!(@customers) do |customer|
  json.extract! customer, :id, :customer_name, :customer_middle_name, :customer_last_name, :customer_address, :customer_city, :customer_zip_code, :state, :customer_phone, :customer_email, :customer_status, :customer_date
  json.url customer_url(customer, format: :json)
end
