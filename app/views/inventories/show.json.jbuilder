json.extract! @inventory, :id, :make, :model, :year, :vin, :color, :wholesale_price, :retail_price, :quantity, :inventory_status, :created_at, :updated_at
