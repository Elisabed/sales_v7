json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :make, :model, :year, :vin, :color, :wholesale_price, :retail_price, :quantity, :inventory_status
  json.url inventory_url(inventory, format: :json)
end
