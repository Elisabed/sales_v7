json.array!(@finances) do |finance|
  json.extract! finance, :id, :interest, :principal, :payment, :interest_rate, :sales_tax, :start_date
  json.url finance_url(finance, format: :json)
end
