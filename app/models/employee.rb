class Employee < ActiveRecord::Base

  has_many :quotes


  validates :employee_first_name, :employee_last_name, :presence =>true

  def employee_info
    "#{employee_first_name}, #{employee_last_name}"
  end

end
