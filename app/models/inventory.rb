class Inventory < ActiveRecord::Base

  belongs_to :quote


  validates :make, :model, :year, :vin, :color, :quantity, :inventory_status, :presence => true




  def inventory_info
    "#{make}, #{model}"
  end

end
